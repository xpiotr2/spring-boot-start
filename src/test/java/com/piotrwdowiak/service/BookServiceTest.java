package com.piotrwdowiak.service;

import com.piotrwdowiak.dao.BookRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.piotrwdowiak.model.Book;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BookServiceTest {

    @MockBean
    private BookRepository bookRepository;

    @Autowired
    private BookService bookService;

    @Test
    public void testAddBook() {
        Book book = new Book();
        bookService.addBook(book);
        verify(bookRepository, times(1)).save(book);
    }

    @Test
    public void testGetBook() {
        Book book = new Book();
        book.setTitle("Harry Potter");
        book.setIsbn("123-123");
        when(bookRepository.findOne("123-123")).thenReturn(book);

        Book result = bookService.getBook("123-123");
        Assert.assertEquals(book.getTitle(), result.getTitle());
        verify(bookRepository, times(1)).findOne("123-123");
    }

}
