package com.piotrwdowiak.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.piotrwdowiak.service.BookService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.piotrwdowiak.model.Book;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest
@RunWith(SpringRunner.class)
public class BookControllerTest {

    public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));
    public static final String ISBN = "1234-5678-90";

    Book book;
    List<Book> bookList = new ArrayList<>();

    @MockBean
    private BookService bookService;

    @Autowired
    private MockMvc mvc;

    @Before
    public void initialize() {
        book = new Book();
        book.setTitle("Harry Potter");
        book.setAuthor("J. K. Rowling");
        book.setIsbn(ISBN);
        bookList = new ArrayList<>();
        bookList.add(book);
        when(bookService.getBooks()).thenReturn(bookList);
        when(bookService.getBook(ISBN)).thenReturn(book);
    }

    @Test
    public void testGetBooks() throws Exception {
        mvc.perform(get("/books").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(mapToJson(bookList)));
    }

    @Test
    public void testGetBook() throws Exception {
        mvc.perform(get("/book/" + ISBN).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().json(mapToJson(book)));
    }

    @Test
    public void testAddBook() throws Exception {
         mvc.perform(post("/books").contentType(APPLICATION_JSON_UTF8)
                .content(mapToJson(book)))
                .andExpect(status().isOk());
    }

    @Test
    public void testDeleteBook() throws Exception {
        mvc.perform(delete("/book/" + ISBN).contentType(APPLICATION_JSON_UTF8))
                .andExpect(status().isOk());
    }

    @Test
    public void testUpdateBook() throws Exception {
        mvc.perform(put("/book/" + ISBN).contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(mapToJson(book)))
                .andExpect(status().isOk());
    }

    private String mapToJson(Object object) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.WRAP_ROOT_VALUE, false);
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();
        return ow.writeValueAsString(object);
    }
}
