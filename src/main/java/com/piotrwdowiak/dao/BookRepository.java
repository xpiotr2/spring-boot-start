package com.piotrwdowiak.dao;

import com.piotrwdowiak.model.Book;
import org.springframework.data.repository.CrudRepository;


public interface BookRepository extends CrudRepository<Book, String> {

}
