package com.piotrwdowiak.controller;

import com.piotrwdowiak.model.Book;
import com.piotrwdowiak.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class BookController {

    @Autowired
    BookService bookService;

    @RequestMapping(value="/books")
    @ResponseBody
    public List<Book> getBooks() {
        return bookService.getBooks();
    }

    @RequestMapping(value = "/book/{id}")
    public Book getBook(@PathVariable String id) {
        return bookService.getBook(id);
    }

    @RequestMapping(value = "/books", method = RequestMethod.POST)
    public void addBook(@RequestBody Book book) {
        bookService.addBook(book);
    }

    @RequestMapping(value = "/book/{id}", method = RequestMethod.PUT)
    public void updateBook(@RequestBody Book book, @PathVariable String id) {
        bookService.updateBook(book);
    }

    @RequestMapping(value = "/book/{id}", method = RequestMethod.DELETE)
    public void deleteBook(@PathVariable String id) {
        bookService.deleteBook(id);
    }

}
