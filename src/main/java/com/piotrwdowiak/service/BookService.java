package com.piotrwdowiak.service;

import com.piotrwdowiak.dao.BookRepository;
import com.piotrwdowiak.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class BookService {

    @Autowired
    BookRepository bookRepository;

    public List<Book> getBooks() {
        final ArrayList<Book> result = new ArrayList<Book>();
        bookRepository.findAll().forEach(book -> result.add(book));
        return result;
    }

    public Book getBook(String id) {
        return bookRepository.findOne(id);
    }

    public void addBook(Book book) {
        bookRepository.save(book);
    }

    public void updateBook(Book book) {
        bookRepository.save(book);
    }

    public void deleteBook(String id) {
        bookRepository.delete(id);
    }
}
